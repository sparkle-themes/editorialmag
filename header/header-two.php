<header id="masthead" class="site-header headertwo" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
	<div class="sticky-menu">
		<div class="sparkle-wrapper">
			<nav class="main-navigation">
				<div class="toggle-button">
					<span class="toggle-bar1"></span>
					<span class="toggle-bar2"></span>
					<span class="toggle-bar3"></span>
				</div>
				<div class="nav-menu">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu'
						) );
					?>
				</div>
			</nav>
		</div>
	</div><!-- STICKY MENU -->

	<?php if( get_theme_mod('editorialmag_topheader', 'enable') == 'enable' ): ?>
	<div class="top-header">
		<div class="sparkle-wrapper">
			<div class="top-nav">
				<div class="date-time"></div>
				<?php
				if( has_nav_menu( 'menu-2') ):
					wp_nav_menu( array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'top-menu',
						'depth'          => 2,
					) );
				endif;
				?>
			</div>
			<div class="top-right">
				<div class="temprature">
					<?php
						$facebook  = get_theme_mod( 'editorialmag_social_facebook' );
						$twitter   = get_theme_mod( 'editorialmag_social_twitter' );
						$linkedin  = get_theme_mod( 'editorialmag_social_linkedin' );
						$youtube   = get_theme_mod( 'editorialmag_social_youtube' );
						$pinterest = get_theme_mod( 'editorialmag_social_pinterest' );
						$vimeo     = get_theme_mod( 'editorialmag_social_vimeo' );
						$flickr     = get_theme_mod( 'editorialmag_social_flickr' );
					?>
					<?php if( !empty( $facebook ) ) { ?>
						<a href="<?php echo esc_url( $facebook ); ?>" target="_blank">
							<i class="icofont fab fa-facebook-f"></i>
						</a>
					<?php } if( !empty( $twitter ) ) { ?>
						<a href="<?php echo esc_url( $twitter ); ?>" target="_blank">
							<i class="icofont fab fa-twitter"></i>
						</a>
					<?php } if( !empty( $youtube ) ) { ?>
						<a href="<?php echo esc_url( $youtube ); ?>" target="_blank">
							<i class="icofont fab fa-youtube"></i>
						</a>
					<?php } if( !empty( $linkedin ) ) { ?>	
						<a href="<?php echo esc_url( $linkedin ); ?>" target="_blank">
							<i class="icofont fab fa-linkedin-in"></i>
						</a>
					<?php } if( !empty( $pinterest ) ) { ?>	
						<a href="<?php echo esc_url( $pinterest ); ?>" target="_blank">
							<i class="icofont fab fa-pinterest"></i>
						</a>
					<?php } if( !empty( $vimeo ) ) { ?>	
						<a href="<?php echo esc_url( $vimeo ); ?>" target="_blank">
							<i class="icofont fab fa-vimeo-v"></i>
						</a>
					<?php } if( !empty( $flickr ) ) { ?>	
						<a href="<?php echo esc_url( $flickr ); ?>" target="_blank">
							<i class="icofont fab fa-flickr"></i>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div> <!-- TOP HEADER -->
	<?php endif; ?>

	<div class="bottom-header">
		<div class="sparkle-wrapper">
			<div class="site-logo site-branding">
				<?php the_custom_logo(); ?>
				<h1 class="site-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<?php bloginfo( 'name' ); ?>
					</a>
				</h1>
				<?php 
					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) { ?>
						<p class="site-description">
							<?php echo $description; /* WPCS: xss ok. */ ?>
						</p>
				<?php } ?>					
			</div> <!-- .site-branding -->

			<div class="header-ad-section">
				<?php 
				    if( is_active_sidebar( 'headerpromo' ) ){
				        dynamic_sidebar( 'headerpromo' );
				    }
				?>
			</div>
		</div>
	</div> <!-- BOTTOM HEADER -->

	<div class="nav-wrap nav-left-align">
		<div class="sparkle-wrapper">
			<nav class="main-navigation">
				<div class="toggle-button">
					<span class="toggle-bar1"></span>
					<span class="toggle-bar2"></span>
					<span class="toggle-bar3"></span>
				</div>
				<div class="nav-menu">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu'
						) );
					?>
				</div>
			</nav>

			<div class="nav-icon-wrap">
				<div class="search-wrap">
					<i class="icofont fas fa-search"></i>
					<div class="search-form-wrap">
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
		<img class="nav-shadow" src="<?php echo esc_url(get_template_directory_uri().'/assets/images/shadow.png'); ?>" alt="Shadow">
	</div> <!-- MAIN NAVIGATION -->
</header>