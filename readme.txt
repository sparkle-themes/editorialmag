=== Editorialmag ===
Contributors: sparklewpthemes
Tags: right-sidebar, left-sidebar, custom-background, custom-menu, custom-colors, sticky-post, threaded-comments, translation-ready, featured-images, theme-options, grid-layout, footer-widgets, news, blog, e-commerce, footer-widgets
Requires at least: 4.7
Tested up to: 6.0.1
Requires PHP: 5.2.4
Stable tag: 1.1.1
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Editorialmag is a user-friendly ultra-fast and cleans free WordPress magazine theme, WordPress magazine Editorialmag theme is specially designed for magazine, News portal, Blog and digital content publishing websites. Editorialmag free magazine is a simple clean modern theme with elegant and responsive design and completely built on the base customizer and widget, where allows you to customize theme settings & layout with live preview, especially in front page three different section which helps you to display post different style with different layout and easily re-ordering each and every post block section, by default Editorialmag free WordPress magazine theme includes 9+ different custom widget, which custom widget you can easily add in 3 different widget area or left or right sidebar widget area or footer widget area which help you to make your website better looks.

== Description ==

Editorialmag is a user-friendly ultra-fast and cleans free WordPress magazine theme, WordPress magazine Editorialmag theme is specially designed for magazine, News portal, Blog and digital content publishing websites. Editorialmag free magazine is a simple clean modern theme with elegant and responsive design and completely built on the base customizer and widget, where allows you to customize theme settings & layout with live preview, especially in front page three different section which helps you to display post different style with different layout and easily re-ordering each and every post block section, by default Editorialmag free WordPress magazine theme includes 9+ different custom widget, which custom widget you can easily add in 3 different widget area or left or right sidebar widget area or footer widget area which help you to make your website better looks. Editorialmag WordPress magazine theme also supports many more 3rd party plugins and compatible with Jetpack, AccessPress Social Share, AccessPress, page builder, WooCommerce, Contact Form 7, Social Counter and many more other plugins. Official Support Forum: http://sparklewpthemes.com/support/ Demo: http://demo.sparklewpthemes.com/editorialmag/demos/ and Docs: http://docs.sparklewpthemes.com/editorialmag


== Translation ==

Editorialmag theme is translation ready.


== Copyright ==

Editorialmag WordPress Theme, Copyright (C) 2018 Sparkle Themes.
Editorialmag is distributed under the terms of the GNU GPL


== Installation ==
    
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like element page builder, Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme all features ? =

You can check our Theme features at http://sparklewpthemes.com/wordpress-themes/editorialmag/

= Where can I find theme demo? =

You can check our Theme Demo at http://demo.sparklewpthemes.com/editorialmag/demos/


== License ==

Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)

respond
Created by Copyright 2011 Scott Jehl, scottjehl.com ( https://github.com/scottjehl/Respond )
MIT License: http://opensource.org/licenses/MIT

html5shiv
Created by @afarkas @jdalton @jon_neal @rem ( https://github.com/afarkas/html5shiv )
MIT License: http://opensource.org/licenses/MIT

Google Fonts - Apache License, version 2.0

Font-Awesome https://github.com/FortAwesome/Font-Awesome [MIT](http://opensource.org/licenses/MIT)- by @davegandy, The Font Awesome font is licensed under the SIL OFL 1.1: http://scripts.sil.org/OFL

jquery-match-height 0.7.2 by @liabru
http://brm.io/jquery-match-height/
License: MIT (http://www.opensource.org/licenses/mit-license.php)

Lightslider - v1.1.6 - 2016-10-25
https://github.com/sachinchoolur/lightslider
Copyright (c) 2016 Sachin N; 
License: MIT (http://www.opensource.org/licenses/mit-license.php)


ImagesLoaded v3.1.8
License: MIT (http://www.opensource.org/licenses/mit-license.php)

Moment.js
Version : 2.18.1
Authors : Tim Wood, Iskren Chernev, Moment.js contributors
License: MIT (http://www.opensource.org/licenses/mit-license.php)


Images

	All the images are used from http://pxhere.com under License CC0 Public Domain or self taken are fully GPL compatible.


	https://pxhere.com/en/photo/633991
	https://pxhere.com/en/photo/418994
	https://pxhere.com/en/photo/418963
	https://pxhere.com/en/photo/418995
	https://pxhere.com/en/photo/457926



== Changelog ==

= 1.1.5 20th March 2020  =

** Add one click import demo data.
** Add new header layout.
** Add new breaking news layout.
** Make theme fully compatible with RTL(Right to left) languages compatible.
** Make theme fully translation ready with compatible polylang & WPML plugins.
** Make theme fully compatible with latest version FontAwesome icon.
** Fixed major responsive and normal design issue.
** .Pot file updates with recent change.
** Add documentation link in customizer.
** Recommended plugin added (Polylang, Elementor, Loco Translate).


= 1.1.9 15th Aug 2022 =
** WordPress 6.0 Compatible
** Sticky Js Removed


= 1.1.7 21st July 2021 =

** WordPress 5.8 Compatible

= 1.1.4 10th July 2018 =

** Fixed responsive & normal design issue.

= 1.1.3 6th May 2018 =

** Change screenshot.
** Re-generate translate .pot file.

= 1.1.2 15th January 2018 =

** Change the "Upsell" function error issue.


= 1.1.1 14th January 2018 =

** Add single post page author section enable/disable options on customizer.
** Add the author single sub title.
** Fixed single post description link color issue.
** Change the "Upsell" button and link


= 1.1.0 30th November 2017 =

** Fixed the widget number increase issue.
** Change screenshot image.
** Fixed the update date meta tags.

= 1.0.9 23rd November 2017 =

** Add plugins recommend TGM library file.
** Add the Upgrade Pro version button on customizer.
** Add new .pot file.


= 1.0.8 23rd November 2017 =

** Fixed admin review.


= 1.0.7 21th November 2017 =

** Fixed textdomain issue.

= 1.0.6 21th November 2017 =

** Fixed all programming and design issue which list by admin review.

= 1.0.5 12th November 2017 =

** Fixed all programming and design issue which list by review.

= 1.0.4 =

** Add the new 2 different widget (popular,comment,tags & timeline posts).
** Fixed the responsive issue.
** Re-generate theme language .pot file. 

= 1.0.3 =

** Remove unlicensed GPL version font icon and add MIT license font icon Font Awesome
** Add new slider widget.

= 1.0.2 =

** Add one banner widget area.
** Fixed the responsive design issue.
** Make the compatible widget in footer and sidebar widget.

= 1.0.1 =

** Re-submitted theme for review in http://wordpress.org

= 1.0.0 =

** Submitted theme for review in http://wordpress.org