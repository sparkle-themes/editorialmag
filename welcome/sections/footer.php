<div class="welcome-upgrade-wrap">
    <div class="welcome-upgrade-header">
        <h3><?php printf(esc_html__('Premium Version of %s', 'editorialmag'), $this->theme_name); ?></h3>
        <p><?php echo sprintf(esc_html__('Check out the demos that you can create with the premium version of the %s theme. 10+ Pre-defined demos can be imported with just one click in the premium version.', 'editorialmag'), $this->theme_name); ?></p>
    </div>

    <div class="recomended-plugin-wrap">
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/editorialmagpro/demos/wp-content/uploads/sites/6/2020/12/pro-demo.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Editorialmag Pro - Main','editorialmag'); ?></span>
                <span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/editorialmagpro/" class="button button-primary"><?php echo esc_html__('Preview', 'editorialmag'); ?></a>
				</span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/editorialmagpro/demos/wp-content/uploads/sites/6/2020/12/pro-glamour.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Editorialmag Pro - Glamour','editorialmag'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/editorialmagpro/glamour/" class="button button-primary"><?php echo esc_html__('Preview', 'editorialmag'); ?></a>
				</span>
            </div>
        </div>
        
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/editorialmagpro/demos/wp-content/uploads/sites/6/2020/12/pro-sport.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Editorialmag Pro - Sport','editorialmag'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/editorialmagpro/sport/" class="button button-primary"><?php echo esc_html__('Preview', 'editorialmag'); ?></a>
				</span>
            </div>
        </div>
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/editorialmagpro/demos/wp-content/uploads/sites/6/2020/12/pro-tech.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Editorialmag Pro - Technology','editorialmag'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/editorialmagpro/tech/" class="button button-primary"><?php echo esc_html__('Preview', 'editorialmag'); ?></a>
				</span>
            </div>
        </div>
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/editorialmagpro/demos/wp-content/uploads/sites/6/2020/12/pro-new.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Editorialmag Pro - News','editorialmag'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/editorialmagpro/news/" class="button button-primary"><?php echo esc_html__('Preview', 'editorialmag'); ?></a>
                </span>
            </div>
        </div>
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/editorialmagpro/demos/wp-content/uploads/sites/6/2020/12/free-demo.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Editorialmag Pro - MagLite','editorialmag'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/editorialmaglite/" class="button button-primary"><?php echo esc_html__('Preview', 'editorialmag'); ?></a>
                </span>
            </div>
        </div>
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/editorialmagpro/demos/wp-content/uploads/sites/6/2020/12/free-sport.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Editorialmag Pro - Sport','editorialmag'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/editorialmag/sport/" class="button button-primary"><?php echo esc_html__('Preview', 'editorialmag'); ?></a>
                </span>
            </div>
        </div>
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/editorialmagpro/demos/wp-content/uploads/sites/6/2020/12/free-tech.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Editorialmag Pro - Technology','editorialmag'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/editorialmag/tech/" class="button button-primary"><?php echo esc_html__('Preview', 'editorialmag'); ?></a>
                </span>
            </div>
        </div>

    </div>
</div>

<div class="welcome-upgrade-box">
    <div class="upgrade-box-text">
        <h3><?php echo esc_html__('Upgrade To Premium Version (14 Days Money Back Guarantee)', 'editorialmag'); ?></h3>
        <p><?php echo sprintf(esc_html__('With %s Pro theme you can create a beautiful website. If you want to unlock more possibilities then upgrade to the premium version, try the Premium version and check if it fits your need or not. If not, we have 14 days money-back guarantee.', 'editorialmag'), $this->theme_name); ?></p>
    </div>

    <a class="upgrade-button" href="https://sparklewpthemes.com/wordpress-themes/editorialmagpro/" target="_blank"><?php esc_html_e('Upgrade Now', 'editorialmag'); ?></a>
</div>

