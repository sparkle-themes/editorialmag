<div class="free-vs-pro-info-wrap">
    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('ONE TIME PAYMENT', 'editorialmag'); ?></h4>
        <p><?php esc_html_e('No renewal needed', 'editorialmag'); ?></p>
    </div>

    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('UNLIMITED DOMAIN LICENCE', 'editorialmag'); ?></h4>
        <p><?php esc_html_e('Use in as many websites as you need', 'editorialmag'); ?></p>
    </div>

    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('FREE UPDATES FOR LIFETIME', 'editorialmag'); ?></h4>
        <p><?php esc_html_e('Keep up to date', 'editorialmag'); ?></p>
    </div>
</div>

<table class="comparison-table">
	<tr>
		<td>
			<span><?php esc_html_e('Upgrade to Pro', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Upgrade to pro version for additional features and better supports.', 'editorialmag'); ?></p>
		</td>
		<td colspan="2">
			<a target="__blank" class="buy-pro-btn" href="https://sparklewpthemes.com/wordpress-themes/editorialmagpro/"><?php esc_html_e('Buy Now ($55 only)', 'editorialmag'); ?></a>
		</td>
	</tr>
	<tr>
		<th><?php esc_html_e('Features', 'editorialmag'); ?></th>
		<th><?php esc_html_e('Free', 'editorialmag'); ?></th>
		<th><?php esc_html_e('Pro', 'editorialmag'); ?></th>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('One Click Demo Import', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Just simple with once click you can import sample content form the demo easily.', 'editorialmag'); ?>
		</td>
		<td><?php esc_html_e('1 Demo Only', 'editorialmag'); ?></td>
		<td><?php esc_html_e('5 Demos', 'editorialmag'); ?></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Multiple Header Layouts and Settings', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Premium version gives the option to choose 4 different header layout per as you want.', 'editorialmag'); ?></p>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('News Ticker Layout', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Scrolling latest & category news of headlines on your website differents 2 layouts.', 'editorialmag'); ?>
		</td>
		<td><?php esc_html_e('1', 'editorialmag'); ?></td>
		<td><?php esc_html_e('3', 'editorialmag'); ?></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Unlimited Color Options', 'editorialmag'); ?></span>
			<p><?php esc_html_e('The free version has a basic color option but the premium version has advanced color options that allow customizing the color theme primary colors.', 'editorialmag'); ?>
		</td>
		<td><?php esc_html_e('Basic', 'editorialmag'); ?></td>
		<td><?php esc_html_e('Advanced', 'editorialmag'); ?></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Remove Footer Credit Text', 'editorialmag'); ?></span>
			<p><?php esc_html_e('The premium version easily allows to remove or change the footer credit text.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Advanced Top Header Settings', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Comes with an option for the top bar where you can add contact info, social icon, nav menu.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Custom Inbuilt Widgets', 'editorialmag'); ?></span>
			<p><?php esc_html_e('With custom widget and Site origin page builder, you can play to create various website layout.', 'editorialmag'); ?>
		</td>
		<td>9+</td>
		<td>17+</td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Post View Count', 'editorialmag'); ?></span>
			<p><?php esc_html_e('The theme has features to count post visiter count.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>


	<tr>
		<td>
			<span><?php esc_html_e('Category Post Layouts', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Display 4 different categories layout options, select any one layout & keep your visitors engaged on your website.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>


	<tr>
		<td>
			<span><?php esc_html_e('Unique Post Format Options', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Single Posts Format options helps you to show single posts different style with video, gallery, audio and (Standard,Classic, full Screen) post.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>


	<tr>
		<td>
			<span><?php esc_html_e('PreLoader Option', 'editorialmag'); ?></span>
			<p><?php esc_html_e('The loading screen that appears until the website is fully loaded. The premium version has the option to choose from 10 preloaders per as you want.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Sidebar Layout Options', 'editorialmag'); ?></span>
			<p><?php esc_html_e('The premium version has the option to change the sidebar layout universally or change the sidebar for individual post/page. Moreover, it allows choosing the sidebar widgets for individual post/page.', 'editorialmag'); ?>
		</td>
		<td><?php esc_html_e('Basic', 'editorialmag'); ?></td>
		<td><?php esc_html_e('Advanced', 'editorialmag'); ?></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Website Layout (Fullwidth or Boxed)', 'editorialmag'); ?></span>
			<p><?php esc_html_e('The premium version has the option to change the website layout to be full width or boxed. Additionally, you can set the width of the website container.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>


	<tr>
		<td>
			<span><?php esc_html_e('WooCommerce Compatible', 'editorialmag'); ?></span>
			<p><?php esc_html_e('The premium Version has enhanced WooCommerce options like adding cart icon in the menu, product page settings, sidebar layout settings and more.', 'editorialmag'); ?>
		</td>
		<td><?php esc_html_e('Basic', 'editorialmag'); ?></td>
		<td><?php esc_html_e('Advanced', 'editorialmag'); ?></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Translation Ready', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Both free and pro version are fully translation ready.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
		<span><?php esc_html_e('Search Engine Optimization', 'editorialmag'); ?></span>
		<p><?php esc_html_e('Follows best SEO practices so that your website always rank higher in Search Engines.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Major Browser Compatible', 'editorialmag'); ?></span>
			<p><?php esc_html_e('The website shows good and runs smoothly in all major browsers.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Responsive - Mobile Friendly', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Adapts to any screen size and display beautifully.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Child Theme Support', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Child theme support makes it easy for you to transfer your settings and options when you update and change you themes.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('SEO Friendly', 'editorialmag'); ?></span>
			<p><?php esc_html_e('SEO refers to search engine optimization, or the process of optimizing a website so that people can easily find it via search engines like Google.', 'editorialmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Fast and Friendly Support', 'editorialmag'); ?></span>
			<p><?php esc_html_e('For Premium theme, the user will get a reply in 10 hours or less.', 'editorialmag'); ?>
		</td>
		<td>-</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Upgrade to Pro', 'editorialmag'); ?></span>
			<p><?php esc_html_e('Upgrade to pro version for additional features and better supports.', 'editorialmag'); ?></p>
		</td>
		<td colspan="2">
			<a target="__blank" class="buy-pro-btn" href="https://sparklewpthemes.com/wordpress-themes/editorialmagpro/"><?php esc_html_e('Buy Now ($55 only)', 'editorialmag'); ?></a>
		</td>
	</tr>

</table>
