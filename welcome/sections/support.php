<div class="support-wrap">
    <div class="support-col">
        <h3><?php echo esc_html__('Documentation', 'editorialmag'); ?></h3>
        <p><?php
            printf(
                    /* translators: Theme Name */
                    esc_html__('Read our theme documentation. our documentation contains all the necessary information required to set up the %s theme with clean screenshout.', 'editorialmag'), esc_html($this->theme_name));
            ?></p>
        <a class="button button-primary" target="_blank" href="https://docs.sparklewpthemes.com/editorialmag/"><?php echo esc_html__('Read Full Documentation', 'editorialmag'); ?></a>
    </div>

    <div class="support-col">
        <h3><?php echo esc_html__('Create Support Tickets', 'editorialmag'); ?></h3>
        <p><?php echo esc_html__('Still, having problems after reading all the documentation? No Problem!! Please create a support ticket. Our dedicated support team will help you to solve your problem.', 'editorialmag'); ?></p>
        <a class="button button-primary" target="_blank" href="https://sparklewpthemes.com/support/"><?php echo esc_html__('Create Support Tickets', 'editorialmag'); ?></a>
    </div>
</div>