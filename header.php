<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Editorialmag
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> <?php editorialmag_html_tag_schema(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'editorialmag' ); ?></a>

	<?php
		/**
		 * Header Before Blank Hooks
		*/ 
		do_action( 'editorialmag_header_before', 5 );

		$header_type = esc_attr( get_theme_mod( 'editorialmag_header_layout','headertwo' ) );
		
		if($header_type == 'headerone'){

			get_template_part('header/header', 'one');

		}else if($header_type == 'headertwo'){

			get_template_part('header/header', 'two');

		}else{ 

			get_template_part('header/header', 'one'); 
		}

		/**
		 * Header After Blank Hooks
		*/ 
		do_action( 'editorialmag_header_after', 10 );

		/**
		 * Header Breaking News Section
		*/ 
		do_action( 'editorialmag_breaking_news', 5 );
	?>	

	<div id="content" class="site-content">
