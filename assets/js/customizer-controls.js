(function($) {
    jQuery(document).ready(function($) {

        /**
         * Customizer Option Auto focus
         */
        jQuery('h3.accordion-section-title').on('click', function() {
            var id = $(this).parent().attr('id');
            var is_panel = id.includes("panel");
            var is_section = id.includes("section");

            if (is_panel) {
                focus_item = id.replace('accordion-panel-', '');
                //console.log(focus_item);
                history.pushState({}, null, '?autofocus[panel]=' + focus_item);
            }
            if (is_section) {
                focus_item = id.replace('accordion-section-', '');
                history.pushState({}, null, '?autofocus[section]=' + focus_item);
            }
        });

        $('.sparkle-customizer').on('click', function(evt) {
            evt.preventDefault();
            section = $(this).data('section');
            if (section) {
                wp.customize.section(section).focus();
            }
        });



    });
})(jQuery);