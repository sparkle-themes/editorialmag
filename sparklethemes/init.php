<?php
/**
 * Main Custom admin functions area
 *
 * @since SparklewpThemes
 *
 * @param Editorialmag
 *
*/

/**
 * Load Custom Admin functions that act independently of the theme functions.
*/
require get_theme_file_path('sparklethemes/functions.php');

/**
 * Custom functions that act independently of the theme header.
*/
require get_theme_file_path('sparklethemes/core/custom-header.php');

/**
 * Custom functions that act independently of the theme templates.
*/
require get_theme_file_path('sparklethemes/core/template-functions.php');

/**
 * Custom template tags for this theme.
*/
require get_theme_file_path('sparklethemes/core/template-tags.php');

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {

   require get_theme_file_path('sparklethemes/core/jetpack.php');

}

/**
 * Customizer additions.
*/
require get_theme_file_path('sparklethemes/customizer/customizer.php');

/**
 * Load widget compatibility field file.
*/
require get_theme_file_path('sparklethemes/widget-fields.php');


/**
 * Load woocommerce hooks file.
*/
if ( editorialmag_is_woocommerce_activated() ) {
	
	require get_theme_file_path('sparklethemes/hooks/woocommerce.php');
}

/**
 * Load in customizer upgrade to pro
*/
require get_theme_file_path('sparklethemes/customizer/customizer-pro/class-customize.php');


/**
 * Load Admin Welcome Page.
 */
if ( is_admin() ) {

    require get_template_directory() . '/welcome/welcome.php';

}
require get_template_directory() . '/sparklethemes/starter-content.php';

