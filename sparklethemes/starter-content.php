<?php
    
function editorialmag_starter_content_setup(){

    add_theme_support( 'starter-content', array(
        'attachments' => array(
            
        ),

        'posts' => array(
            
        ),
        
        'options' => array(
            // Our Custom
            'blogdescription' => 'Just another WordPress site ',
            
        ),

        'theme_mods'  => array(
            'editorialmag_social_facebook' => '#',
            'editorialmag_social_twitter' => '#',
            'editorialmag_social_linkedin' => '#',
            'editorialmag_social_youtube' => '#',
            'editorialmag_social_instagram' => '#'
            
        ),

        
    ));
}
add_action( 'after_setup_theme', 'editorialmag_starter_content_setup' );